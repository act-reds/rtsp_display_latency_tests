#include "opencv2/opencv.hpp"
#include <iostream>
#include <string>

using namespace std;
using namespace cv;


int main() {
    const string url = "rtsp://192.168.42.129:8554/fpv_stream";
    cv::VideoCapture capture(url);

    if (!capture.isOpened()) {
        //Error
    }

    cv::namedWindow("TEST");

    cv::Mat frame;

    while(true) {
        if (!capture.read(frame)) {
            //Error
        }
        cv::imshow("TEST", frame);

        cv::waitKey(1);
    }
}


// int main(){
//
//     // Create a VideoCapture object and open the input file
//     // If the input is the web camera, pass 0 instead of the video file name
//     VideoCapture cap;
//     //VideoCapture cap("../chaplin.mp4");
//     cap = VideoCapture("rtsp://192.168.42.129:8554/fpv_stream");
//     int i;
//     Mat frame,img;
//
//     // Check if camera opened successfully
//     if(!cap.isOpened()){
//         cout << "Error opening video stream or file" << endl;
//         return -1;
//     }
//
//     while(1)
//     {
//         cap.read(frame);
//         if (frame.empty()) break;
//         imshow("video", frame);
//         char filename[80];
// //        sprintf(filename,"../image_save/test_%d.png",i++);
// //        imwrite(filename, frame);
//         char key = waitKey(1);
//         if (key == 27) break;
//     }
//
//
//     // When everything done, release the video capture object
//     cap.release();
//
//     // Closes all the frames
//     destroyAllWindows();
//
//     return 0;
// }
